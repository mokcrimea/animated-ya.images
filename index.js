// ==UserScript==
// @name         Animated YA.Images
// @version      0.10
// @description  Animating gif's at yandex images search
// @author       mokcrimea
// @include      *yandex.ru/images/search?*
// @grant        none
// @updateURL    https://bitbucket.org/mokcrimea/animated-ya.images/raw/master/index.js
// @downloadURL  https://bitbucket.org/mokcrimea/animated-ya.images/raw/master/index.js
// ==/UserScript==

(function() {
    'use strict';

    var step = BEM && BEM.blocks && BEM.blocks.uri.getFlatParams().step || 0;

    function animate(i, elem) {
        if (typeof i !== 'number') {
            elem = i;
            i = undefined;
        }

        var imgURL = JSON.parse(elem.dataset.bem)['serp-item'].preview[0].url,
            thumb = elem.querySelector('.serp-item__thumb');

        if ((!i || i % step === 0) && imgURL.match(/\.gif$/i) && !thumb.className.match('animated')) {
            thumb.src = imgURL;
            thumb.onerror = function() {
                var serpItem = JSON.parse(this.parentNode.parentNode.parentNode.dataset.bem)['serp-item'],
                    imgURL = serpItem.preview[1].url,
                    thumbURL = serpItem.thumb.url;

                if (this.src !== imgURL) {
                    this.src = imgURL;
                } else {
                    this.src = thumbURL;
                }
            };
            thumb.className += ' animated';
        }

    }

    function stopAnimate(i, elem) {
        if (typeof i !== 'number') {
            elem = i;
            i = undefined;
        }

        var serpItem = JSON.parse(elem.dataset.bem)['serp-item'],
            thumb = elem.querySelector('.serp-item__thumb');

        thumb.src = serpItem.thumb.url;
        thumb.className = thumb.className.replace('animated', '');

    }

    setInterval(function() {
        $('.serp-item').on({
            mouseenter: function () {
                animate(this);
            },
            mouseleave: function() {
                stopAnimate(this);
            }
        });
        $('.serp-item').each(animate.bind(this));
    }, 1200);

})();
